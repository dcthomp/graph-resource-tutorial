// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "Resource.h"

namespace sketch
{

Resource::Resource(const smtk::common::UUID& uid, smtk::resource::Manager::Ptr manager)
  : Superclass(uid, manager)
{
  this->initialize();
}

Resource::Resource(smtk::resource::Manager::Ptr manager)
  : Superclass(manager)
{
  this->initialize();
}

std::function<bool(const smtk::resource::Component&)> Resource::queryOperation(
  const std::string& query) const
{
  // TODO: We could add grammar here instead of passing it to our parent.
  return this->Superclass::queryOperation(query);
}

void Resource::initialize()
{
  // TODO: Register any queries here.
}

} // namespace sketch
