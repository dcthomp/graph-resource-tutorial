// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step1_Traits_h
#define sketch_step1_Traits_h

#include "Exports.h"

#include "smtk/graph/Component.h"

#include <tuple>
#include <type_traits> // for std::true_type

namespace sketch
{

// Forward-declare node types

class Cusp;
class Group;
class Path;
class Style;

// Declare arc traits

/// Paths can be grouped together.
struct GroupsToPaths
{
  using FromType = Group;
  using ToType = Path;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 group.
};

/// Paths can be smooth, closed curves (no cusps) or have cusps (at ends or along interior).
struct PathsToCusps
{
  using FromType = Path;
  using ToType = Cusp;
  using Directed = std::true_type;
};

/// Paths can have a style.
struct StylesToPaths
{
  using FromType = Style;
  using ToType = Path;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 style.
};

/// Node and arc types for hand drawings.
struct SKETCH_EXPORT Traits
{
  using NodeTypes = std::tuple<Cusp, Group, Path, Style>;
  using ArcTypes = std::tuple<GroupsToPaths, PathsToCusps, StylesToPaths>;
};

class SKETCH_EXPORT Node : public smtk::graph::Component
{
public:
  smtkTypeMacro(sketch::Node);
  smtkSuperclassMacro(smtk::graph::Component);

  template<typename... Args>
  Node(Args&&... args)
    : smtk::graph::Component(std::forward<Args>(args)...)
  {
  }

  std::string name() const override
  {
    if (this->properties().contains<std::string>("name"))
    {
      return this->properties().at<std::string>("name");
    }
    return this->Superclass::name();
  }
  void setName(const std::string& nodeName)
  {
    this->properties().get<std::string>()["name"] = nodeName;
  }
};

/// A node representing a C-0 point on a path (i.e., a corner or endpoint).
class SKETCH_EXPORT Cusp : public Node
{
public:
  smtkTypeMacro(sketch::Cusp);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Cusp(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing a collection of paths.
class SKETCH_EXPORT Group : public Node
{
public:
  smtkTypeMacro(sketch::Group);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Group(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing a single stroke of a pen.
class SKETCH_EXPORT Path : public Node
{
public:
  smtkTypeMacro(sketch::Path);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Path(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing the drawing style for a path (e.g. color, thickness, ...)
class SKETCH_EXPORT Style : public Node
{
public:
  smtkTypeMacro(sketch::Style);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Style(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  void setLineColor(const std::array<double, 3>& color)
  {
    for (int ii = 0; ii < 3; ++ii) { m_lineColor[ii] = color[ii]; }
  }

  void setLineThickness(double thickness)
  {
    this->properties().get<double>()["line_thickness"] = thickness;
  }

protected:
  std::array<double, 3> m_lineColor;
};

} // namespace sketch

#endif // sketch_step1_Traits_h
