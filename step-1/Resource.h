// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step1_Resource_h
#define sketch_step1_Resource_h

#include "Traits.h"
#include "smtk/graph/Resource.h"

namespace sketch
{

using SketchResource = smtk::graph::Resource<Traits>;

/// Represent hand drawings.
class SKETCH_EXPORT Resource
  : public smtk::resource::DerivedFrom<sketch::Resource, SketchResource>
{
public:
  smtkTypeMacro(sketch::Resource);
  smtkSuperclassMacro(smtk::resource::DerivedFrom<Resource, SketchResource>);
  smtkSharedPtrCreateMacro(smtk::resource::PersistentObject);

  Resource(const Resource&) = delete;
  virtual ~Resource() = default;

  /// Re-export our base class's template-method for creating nodes.
  template<typename NodeType>
  std::shared_ptr<NodeType> create()
  {
    return this->SketchResource::create<NodeType>();
  }

  /// Return a boolean functor that classifies components according to \a query.
  std::function<bool(const smtk::resource::Component&)> queryOperation(
    const std::string& query) const override;

protected:
  Resource(const smtk::common::UUID&, smtk::resource::Manager::Ptr manager = nullptr);
  Resource(smtk::resource::Manager::Ptr manager = nullptr);

  void initialize(); // Initialization common to all constructors.
};

} // namespace sketch

#endif // sketch_step1_Resource_h
