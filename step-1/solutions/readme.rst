Solutions
---------

0. You should see a graph connecting path1 and path2 to group; path1 to plain; and path2 to fancy.
   This solutions directory has an image named ``exercise0.png`` that should match yours.

1. Look in the ``testing/cxx/Exercise1.cxx`` file for the solution.
   This will be built by default, so you can inspect the output by running the test:

   .. code-block:: bash

      ./step-1/testing/cxx/UnitTests_step-1_testing_cxx Exercise1 > graph.dot
      dot -Tpng graph.dot > graph.png
      eog graph.png

2. This involves modifying the ``Traits.h`` file. See the ``Traits_Exercise2.h``
   file. You can copy it on top of ``Traits.h``, uncomment the code in ``Exercise2.cxx``,
   rebuild, and run Exercise2 to see it work.

3. Two alternatives are:

   a. Add a new ``CuspsToStyles`` arc type with ``FromType = Cusp`` and ``ToType = Style``.

   b. Make ``Cusp`` and ``Path`` derive from a new class (let's call it ``Shape``) and
      change the ``PathsToStyles`` arc to use the base ``Shape`` class as its ``FromType``.

   While the first option might seem easier at first, the more arc types you add, the
   more difficult it is to get all the nodes to which a ``Style`` applies – because you
   must iterate over all the possible arc types to fetch them.

   The latter option works in this case but note you only get to "spend" inheritance on one
   relationship; if you use inheritance to simplify the types of nodes that styles
   apply to, what happens if you want ``Group`` nodes to connect to ``Path`` nodes
   and other ``Group`` nodes but not ``Cusp`` nodes (i.e., you want to allow groups of groups)?

   Overall, using inheritance to relate node types that are specializations
   of the same concept is preferable for other reasons, so the new ``Traits_Exercise3.h``
   uses the second approach. If you want to go farther and support groups of groups,
   then you'll need to introduce a new arc type for this (arguably distinct) type of
   relationship.
   As above, copy the ``Traits_Exercise3.h`` file onto ``Traits.h`` and uncomment
   the code in ``Exercise3.cxx`` to see the solution in action.

4. You should get a build error. The ``Exercise1.cxx`` file in this directory
   includes code you can uncomment that will cause the following build error
   using GCC's c++ compiler:

   .. code-block:: text

      /usr/bin/c++ -DBOOST_ALL_NO_LIB -DBOOST_DATE_TIME_DYN_LINK -DBOOST_FILESYSTEM_DYN_LINK -DBOOST_SYSTEM_DYN_LINK -DH5_BUILT_AS_DYNAMIC_LIB -DSCRATCH_DIR=\"/stage/build/smtk/graph-tutorial/Testing/Temporary\" -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -D_POSIX_C_SOURCE=200809L -I/stage/build/smtk/graph-tutorial/step-1/testing/cxx -I/stage/source/smtk/graph-tutorial/step-1 -I/stage/build/smtk/graph-tutorial/step-1 -isystem /stage/build/aeva/superbuild/install/include -isystem /stage/build/aeva/superbuild/install/include/smtk/22.05.100 -g -std=c++11 -MD -MT step-1/testing/cxx/CMakeFiles/UnitTests_step-1_testing_cxx.dir/Exercise1.cxx.o -MF step-1/testing/cxx/CMakeFiles/UnitTests_step-1_testing_cxx.dir/Exercise1.cxx.o.d -o step-1/testing/cxx/CMakeFiles/UnitTests_step-1_testing_cxx.dir/Exercise1.cxx.o -c /stage/source/smtk/graph-tutorial/step-1/testing/cxx/Exercise1.cxx
      /stage/source/smtk/graph-tutorial/step-1/testing/cxx/Exercise1.cxx: In function ‘int Exercise1(int, char**)’:
      /stage/source/smtk/graph-tutorial/step-1/testing/cxx/Exercise1.cxx:35:43: error: no matching function for call to ‘smtk::graph::ArcEndpointInterface<sketch::StylesToPaths, smtk::graph::ArcConstness<false>, smtk::graph::ArcDirection<true> >::connect(std::shared_ptr<sketch::Cusp>&)’
         35 |   fancy->outgoing<StylesToPaths>().connect(cusp1);
            |   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~~~
      In file included from /stage/build/aeva/superbuild/install/include/smtk/22.05.100/smtk/graph/ArcMap.h:18,
                       from /stage/build/aeva/superbuild/install/include/smtk/22.05.100/smtk/graph/Component.h:19,
                       from /stage/source/smtk/graph-tutorial/step-1/Traits.h:7,
                       from /stage/source/smtk/graph-tutorial/step-1/Resource.h:5,
                       from /stage/source/smtk/graph-tutorial/step-1/testing/cxx/Exercise1.cxx:1:
      /stage/build/aeva/superbuild/install/include/smtk/22.05.100/smtk/graph/ArcImplementation.h:732:8: note: candidate: ‘bool smtk::graph::ArcEndpointInterface<TraitsType, Const, Outgoing>::connect(const OtherType*) [with TraitsType = sketch::StylesToPaths; Const = smtk::graph::ArcConstness<false>; Outgoing = smtk::graph::ArcDirection<true>; smtk::graph::ArcEndpointInterface<TraitsType, Const, Outgoing>::OtherType = sketch::Path]’
        732 |   bool connect(const OtherType* other)
            |        ^~~~~~~
      /stage/build/aeva/superbuild/install/include/smtk/22.05.100/smtk/graph/ArcImplementation.h:732:33: note:   no known conversion for argument 1 from ‘std::shared_ptr<sketch::Cusp>’ to ‘const OtherType*’ {aka ‘const sketch::Path*’}
        732 |   bool connect(const OtherType* other)
            |                ~~~~~~~~~~~~~~~~~^~~~~
      /stage/build/aeva/superbuild/install/include/smtk/22.05.100/smtk/graph/ArcImplementation.h:739:8: note: candidate: ‘bool smtk::graph::ArcEndpointInterface<TraitsType, Const, Outgoing>::connect(const std::shared_ptr<typename std::conditional<Outgoing::value, typename smtk::graph::ArcImplementation<TraitsType>::ToType, typename smtk::graph::ArcImplementation<TraitsType>::FromType>::type>&) [with TraitsType = sketch::StylesToPaths; Const = smtk::graph::ArcConstness<false>; Outgoing = smtk::graph::ArcDirection<true>; typename std::conditional<Outgoing::value, typename smtk::graph::ArcImplementation<TraitsType>::ToType, typename smtk::graph::ArcImplementation<TraitsType>::FromType>::type = sketch::Path; typename smtk::graph::ArcImplementation<TraitsType>::FromType = sketch::Style; typename smtk::graph::ArcImplementation<TraitsType>::ToType = sketch::Path]’
        739 |   bool connect(const std::shared_ptr<OtherType>& other) { return this->connect(other.get()); }
            |        ^~~~~~~
      /stage/build/aeva/superbuild/install/include/smtk/22.05.100/smtk/graph/ArcImplementation.h:739:50: note:   no known conversion for argument 1 from ‘std::shared_ptr<sketch::Cusp>’ to ‘const std::shared_ptr<sketch::Path>&’
        739 |   bool connect(const std::shared_ptr<OtherType>& other) { return this->connect(other.get()); }
            |                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~

