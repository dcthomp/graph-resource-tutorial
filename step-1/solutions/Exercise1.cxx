#include "Resource.h"

#include <memory>

int Exercise1(int, char**)
{
  using GroupsToPaths = sketch::GroupsToPaths;
  using StylesToPaths = sketch::StylesToPaths;
  using PathsToCusps = sketch::PathsToCusps;

  auto resource = sketch::Resource::create();
  resource->setName("example");

  // Create some nodes.
  auto path1 = resource->create<sketch::Path>();
  auto path2 = resource->create<sketch::Path>();
  auto group = resource->create<sketch::Group>();
  auto plain = resource->create<sketch::Style>();
  auto fancy = resource->create<sketch::Style>();
  path1->setName("path 1");
  path2->setName("path 2");
  group->setName("group");
  plain->setName("plain style");
  fancy->setLineColor({0.6, 0.2, 0.2});
  fancy->setLineThickness(2.0);
  fancy->setName("fancy style");

  // Add some cusp nodes
  auto cusp1 = resource->create<sketch::Cusp>();
  auto cusp2 = resource->create<sketch::Cusp>();
  cusp1->setName("endpoint 1");
  cusp2->setName("endpoint 2");
  path1->outgoing<PathsToCusps>().connect(cusp1);
  path1->outgoing<PathsToCusps>().connect(cusp2);
  // Uncomment this line for exercise 4.
  fancy->outgoing<StylesToPaths>().connect(cusp1);

  // Connect the nodes into a graph.
  group->outgoing<GroupsToPaths>().connect(path1);
  group->outgoing<GroupsToPaths>().connect(path2);
  plain->outgoing<StylesToPaths>().connect(path1);
  fancy->outgoing<StylesToPaths>().connect(path2);

  // Dump the resulting graph.
  resource->dump("", "text/vnd.graphviz");
  return 0;
}
