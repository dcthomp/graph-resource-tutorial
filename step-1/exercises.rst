Exercises
---------

0. Build and run the ``StartingPoint`` test.
   Note that you can inspect the result of the test like so:

   .. code-block:: bash

      ./step-1/testing/cxx/UnitTests_step-1_testing_cxx StartingPoint > graph.dot
      dot -Tpng graph.dot > graph.png
      eog graph.png


   The first line in the script above saves the test output to a file.
   The second line runs dot (a tool that comes with graphviz) to generate
   a layout of the graph and save an image of the result.
   The third line above displays the image.

1. Modify the ``Exercise1`` test and to create two cusps as endpoints
   for ``path1``.

2. Modify the ``Cusp`` class to hold an (x,y)-tuple as coordinates.
   Try using properties as well as a protected member variable.
   Note that the ``Exercise2`` test has some commented-out lines
   that will assign coordinates to the cusps from exercise 1 above.

3. Provide a way for ``Style`` nodes to connect to ``Cusp`` nodes as
   well as ``Path`` nodes (but not to ``Group`` nodes).
   *Hint: there are several ways to do this. What are the
   advantages and disadvantages of each?*

4. Extra credit: Try connecting the plain or fancy style objects to the cusps you
   created for exercise 1 above. Do you get build errors? runtime errors?
   Knowing what these errors look like will help you debug problems with more
   complex resources in the future.
