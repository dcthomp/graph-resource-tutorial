# graph tutorial License Version 1.0

Copyright (c) 2022 Kitware Inc.<br>
1712 Route 9, Suite 300<br>
Clifton Park, NY, 12065, USA.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

 * Neither the name of Kitware nor the names of any contributors may
   be used to endorse or promote products derived from this software
   without specific prior written permission.

**This software is provided by the copyright holders and contributors ``as is''
and any express or implied warranties, including, but not limited to, the
implied warranties of merchantability and fitness for a particular purpose
are disclaimed. In no event shall the authors or contributors be liable for
any direct, indirect, incidental, special, exemplary, or consequential
damages (including, but not limited to, procurement of substitute goods or
services; loss of use, data, or profits; or business interruption) however
caused and on any theory of liability, whether in contract, strict liability,
or tort (including negligence or otherwise) arising in any way out of the use
of this software, even if advised of the possibility of such damage.**

The following files and directories come from third parties. Check the
contents of these for details on the specifics of their respective
licenses.

---

    thirdparty/*
